


module.exports = function(sequelize, DataType){

var server_model;

server_model = sequelize.define('server_model',{
  name:{
    type: DataType.STRING,
    field:'name'
  },
  detail:{
    type: DataType.TEXT,
    field:'detail'
  },
  uid:{
    type: DataType.STRING,
    field:'uid'
  }
},{
  freezeTableName:false
});

server_model.sync({force:true}).then(function(){

  return server_model.create({
    name:'S7uyz',
    detail:'simple stuff',
    uid:'jksdj93u4i3jkjsd8jdkj34k2jkldjs'
  });


});

return server_model;
};
