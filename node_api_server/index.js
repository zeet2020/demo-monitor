var express = require('express');

var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));


var models = require('./models/index');

app.get('/',function(req,res,next){

   res.send("web service");

});

app.get('/validate/:uid',function(req,res,next){

  var uid = req.params.uid;
  var server_model = models.server_model;
  var memory_model = models.memory_model;
  var cpu_model = models.cpu_model;
  var process_model = models.process_model;
  server_model.find({
    uid:uid
    }).then(function(record){

        res.send(record.dataValues);
    });

  //cpu load

  cpu_model.count({
    uid:uid
    }).then(function(count){

      if(count >= 10){
          cpu_model.findOne({
          uid:uid
      }).then(function(record){
           record.destroy();
      });
    }

  });

  memory_model.count({
    uid:uid
  }).then(function(count){

    if(count >= 10){
      memory_model.findOne({
        uid:uid
      }).then(function(record){
           record.destroy();
      });
    }

  });

  process_model.count({
    uid:uid
  }).then(function(count){

    if(count >= 10){
      process_model.findOne({
        uid:uid
      }).then(function(record){
           record.destroy();
      });
    }

  });



});



app.post('/post_data/:uid',function(req,res,next){
  var uid = req.params.uid;
  //console.log(req.params);
  var server_model = models.server_model;
  var memory_model = models.memory_model;
  var cpu_model = models.cpu_model;
  var process_model = models.process_model;
  server_model.find({
    uid:uid
  }).then(function(record){
    var sid = record.dataValues.id;


    cpu_model.create({
      value:req.body.cpu,
      sid:sid
    });

    memory_model.create({
      value:req.body.disk_usage,
      sid:sid
    });
    process_model.create({
      value:req.body.process_count,
      sid:sid
    });

    res.send("done");
  });
  //console.log(req.body);

});





var server = app.listen(1333,function(){
console.log("Server started on port 1333");
});
