

module.exports = function(sequelize,DataType){
  var memory_model = sequelize.define('memory_model',{
      value:{
        type:DataType.STRING,
        field:'value'
      },
      sid:{
        type:DataType.INTEGER,
        field:'sid'
      }
      },{
        freezeTableName:false
      });
      memory_model.sync({force:true});

  return memory_model;
};
