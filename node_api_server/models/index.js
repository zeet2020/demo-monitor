var Sequelize = require('sequelize');
var fs = require('fs');
var path = require('path');
var exp = {}

var sequelize = new Sequelize('node_api','root','root',{
  host:'localhost',
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  }
});

fs
  .readdirSync('./models')
  .filter(function(file){
    return (file.indexOf(".")!==0) && (file !== "index.js")
  })
  .forEach(function(file){
    console.log(path.join(__dirname,file));
    var model = sequelize.import(path.join(__dirname,file));
    exp[model.name] = model;
  });




  exp['sequelize'] = sequelize;
  exp['Sequelize'] = Sequelize;

module.exports = exp;
