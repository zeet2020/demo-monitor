#!/usr/bin/env python
import logging
import sys, time, os
from daemon import Daemon

from system_monitor import Agent


class Watcher(Daemon):
	def run(self):
			a = Agent('/home/zeeshan/daemon/process/agent.json')
			c = 0
			while True:
				f =  a.run()

				file('/tmp/z.txt','w+').write(str(f)+str(c))
				logging.error("well")
				c = c + 1
				time.sleep(5)



if __name__ == "__main__":
	daemon = Watcher('/tmp/daemon.pid')

	if len(sys.argv) == 2:
		if 'start' == sys.argv[1]:
				daemon.start()
		elif 'stop' == sys.argv[1]:
				daemon.stop()
		elif 'restart' == sys.argv[1]:
				daemon.restart()
		else:
				print "unknown command"
				sys.exit(2)

		sys.exit(0)
	else:
		print "usage: %s start|stop|restart" % sys.argv[0]
		sys.exit(2)
