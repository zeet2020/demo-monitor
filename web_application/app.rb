require 'rubygems'
require 'sinatra'
require 'data_mapper'
require 'dm-mysql-adapter'
require 'erb'
require 'json'





set :views, File.expand_path('./application/')
set :public, File.expand_path('./application/')



DataMapper.setup(:default,{
    :adapter => 'mysql',
    :host => 'localhost',
    :username => 'root',
    :password => 'root',
    :database => 'node_api'
  })



  class  Cpu_model
     include DataMapper::Resource

     property :id, Serial
     property :sid, Integer
     property :value, String

  	#DataMapper.finalize
  end

  class  Memory_model
     include DataMapper::Resource

     property :id, Serial
     property :sid, Integer
     property :value, String

  	#DataMapper.finalize
  end

  class  Process_model
     include DataMapper::Resource

     property :id, Serial
     property :sid, Integer
     property :value, String
     property :createdAt, DateTime, :field => "createdAt"

  	#DataMapper.finalize
  end


    class  Server_model
       include DataMapper::Resource

       property :id, Serial
       property :name, String
       property :detail, Text
       property :uid, String

    	#DataMapper.finalize
    end

  #DataMapper.auto_upgrade!






  get '/' do

    erb :index

  end


  get '/cpu' do

    cpu = Cpu_model.all
    cpu.to_json

  end

  get '/memory' do

    memory = Memory_model.all
    memory.to_json

  end

  get '/process' do

    process = Process_model.all
    process.to_json
  end

  get '/server' do

    server = Server_model.all
    server.to_json
  end

  get '/server/:id' do |id|
    record = []
    server = Server_model.get(id)
    memory = Memory_model.all
    process = Process_model.all
    cpu = Cpu_model.all

    if cpu.length > 5
      len = 5
    else
      len = 0
    end
    #return process.to_json
    len.times do |i|
      obj = {}
      obj['name'] = server.name
      obj['cpu'] = cpu[i].value
      obj['memory'] = memory[i].value
      obj['process'] = process[i].value
      obj['createdAt'] = process[i].createdAt
      record.push(obj)
    end

    record.to_json
  end
