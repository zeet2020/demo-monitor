import psutil
import time, sys, urllib2, json, urllib




class Cmd:

    def __init__(self):
            self.inter_queue = []


    def __process_command(self):
            cmd_obj = self.inter_queue[0]
            del self.inter_queue[0]
            if cmd_obj['type'] == 'sys_cmd':
                    os.system(str(cmd_obj['cmd']))

class monitor:

    def __init__(self):
            self.cpu_load_list = []
            self.memory_list = []
            self.pids_list = []
            #self.data = []
            self.record()

    def getpidcount(self):
            return len(self.pids_list)

    def __record_pid(self):
            self.pids_list = psutil.pids()

    def __record_cpu(self):
            cpu_load = psutil.cpu_percent(interval=1)
            if len(self.cpu_load_list) > 5:
                del self.cpu_load_list[0]
            self.cpu_load_list.append(cpu_load)

    def __record_memory(self):
            disk_load = psutil.virtual_memory().percent
            if len(self.memory_list) > 5:
                del self.memoty_list[0]
            self.memory_list.append(disk_load)

    def record(self):
            try:
                self.__record_pid()
                self.__record_cpu()
                self.__record_memory()
            except Exception, e:
                print e
                print "some error"

    def toHash(self):
            return {'disk_usage':self.memory_list[0],'cpu':self.cpu_load_list[0],'process_count':self.getpidcount()}



class Agent:

    def __init__(self,config_name):
            self.config_name = config_name
            self.config = {}
            self.serverUidisValid = False
            self.isReachable = False
            self.__load_config()


    def __load_config(self):
            flag = False
            try:
                with open(self.config_name) as data:
                    self.config = json.load(data)
                    flag = True
            except Exception,e:
                return e

            return flag

    def check_reachable(self):
            self.__validate_server_uid()
            if self.serverUidisValid:
                self.isReachable = True
            return self.isReachable

    def __validate_server_uid(self):
            uniq_id = self.config['uid']
            url = self.config['host']+self.config['validation_path']+'/'+self.config['uid']
            try:
                res = urllib2.urlopen(url)
                if res.getcode() == 200:
                    self.serverUidisValid = True
            except Exception,e:
                self.serverUidisValid = False

    def __post_status(self,data):
            url = self.config['host']+self.config['post_path']+'/'+self.config['uid']
            data = urllib.urlencode(data)
            try:
                req = urllib2.Request(url=url,data=data)
                res = urllib2.urlopen(req)
                if res.getcode() == 200:
                    return res.read()
            except Exception,e:
                print "failed to post the data"

    def run(self):

            if not self.__load_config():
                return False
            self.check_reachable()

            if self.isReachable and self.serverUidisValid:
                m = monitor()
                m.record()
                data = m.toHash()
                d = self.__post_status(data)

                return d

            return False
