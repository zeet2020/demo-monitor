module.exports = function(sequelize,DataType){

var process_model = sequelize.define('process_model',{
  value:{
    type:DataType.STRING,
    field:'value'
  },
  sid:{
    type:DataType.INTEGER,
    field:'sid'
  }
  },{
    freezeTableName:false
  });
  process_model.sync({force:true});

  return process_model;
};
