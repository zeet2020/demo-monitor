module.exports = function(sequelize,DataType){




var cpu_model = sequelize.define('cpu_model',{
  value:{
    type:DataType.STRING,
    field:'value'
  },
  sid:{
    type:DataType.INTEGER,
    field:'sid'
  }
},{
  freezeTableName:false
});
cpu_model.sync({force:true});

return cpu_model;
};
